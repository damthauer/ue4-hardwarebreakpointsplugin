// Copyright 2020 Daniel Amthauer. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class HWBP_ExampleEditorTarget : TargetRules
{
	public HWBP_ExampleEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "HWBP_Example" } );
	}
}
