// Copyright 2020 Daniel Amthauer. All Rights Reserved.

#include "HWBP_Example.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HWBP_Example, "HWBP_Example" );
