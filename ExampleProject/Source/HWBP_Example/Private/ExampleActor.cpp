// Copyright 2020 Daniel Amthauer. All Rights Reserved.

#include "ExampleActor.h"
#include "PropertyHelpers.h"

AExampleActor::AExampleActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

bool AExampleActor::TestFunc(int Func)
{
	UE_LOG(LogTemp, Log, TEXT(""));
	return true;
}

void AExampleActor::TestCallstack()
{
	TestCallstackBreakpoint = FPlatformHardwareBreakpoints::SetNativeFunctionHardwareBreakpoint(&AExampleActor::TestCallstackReturn);
	TestBlueprintCallstack();
}

bool AExampleActor::TestCallstackReturn()
{
	FPlatformHardwareBreakpoints::RemoveHardwareBreakpoint(TestCallstackBreakpoint);
	return true;
}

//This is here so you can have more detailed info when inspecting this with a debugger
PRAGMA_DISABLE_OPTIMIZATION
void AExampleActor::BeginPlay()
{
	Super::BeginPlay();
	{
		Vectors.Add(FVector::ZeroVector);
		Vectors.Add(FVector::ZeroVector);
		
		//You can set a data breakpoint using the property system (this is what the BP node does), this is useful when the property is defined in BP instead of code
		auto Vectors1ZAddress = PropertyHelpers::FindPropertyAddress(this, GetClass(), TEXT("Vectors[1].Z"));
		//You are not forced to use FScopedHardwareBreakpoint, but it makes it easier so you don't forget to remove the breakpoint after it loses scope
		//You can use the returned DebugRegisterIndex to clear it by hand if you prefer
		FScopedHardwareBreakpoint DataBreakpoint = FPlatformHardwareBreakpoints::SetDataBreakpoint(Vectors1ZAddress.Address, Vectors1ZAddress.Property->GetSize());
		Vectors[1].Z = 2.0f; //The breakpoint will fire here

		//Or you can set a data breakpoint directly, and it will infer the data breakpoint size to set (up to 8 bytes) from the type's size
		FScopedHardwareBreakpoint YBreakpoint = FPlatformHardwareBreakpoints::SetDataBreakpoint(&Vectors[1].Y);
		Vectors[1].Y = 1.0f;

		//And even better, you can set a data breakpoint with a native condition of your choice. The condition remembers the type so you don't have to do any tedious conversion
		//But it has to match the type you passed in for the address or it won't compile.
		FScopedHardwareBreakpoint XBreakpoint = FPlatformHardwareBreakpoints::SetDataBreakpointWithCondition(&Vectors[1].X, [](const float& LastValue, const float& Value) 
		{
			return FMath::IsNaN(Value); 
		});
		Vectors[1].X = 1.0f; //The breakpoint will not fire here, because 1.0f is not a NaN value
		Vectors[1].X = NAN; // It will fire here however

		//You can set a breakpoint on any native function, however this only works for all invocations 
		//if the function in question is not inlined. To make sure of that, place FORCENOINLINE in front of the function definition
		FScopedHardwareBreakpoint FunctionBreakpoint = FPlatformHardwareBreakpoints::SetNativeFunctionHardwareBreakpoint(&AExampleActor::TestFunc);
		TestFunc(0);

		//You can check if a specific breakpoint is set
		bool Specific = FPlatformHardwareBreakpoints::IsBreakpointSet(FunctionBreakpoint);
	}
	//Or check whether any breakpoints are set
	//None should be set at this point because they are all out of scope
	bool Any = FPlatformHardwareBreakpoints::AnyBreakpointSet();
	Vectors[1].Z = 0.0f; //Should no longer fire a breakpoint
	TestFunc(0);//Should no longer fire a breakpoint
	TestCallstack();
}
PRAGMA_ENABLE_OPTIMIZATION

