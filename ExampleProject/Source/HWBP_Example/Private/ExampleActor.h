// Copyright 2020 Daniel Amthauer. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "HAL/PlatformHardwareBreakpoints.h"
#include "ExampleActor.generated.h"

UCLASS(Blueprintable)
class AExampleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AExampleActor();

	//Use FORCENOINLINE if you want all invocations of your function to be caught by hardware breakpoints
	UFUNCTION(BlueprintCallable, Category = "Test")
	FORCENOINLINE bool TestFunc(int Func);

	UPROPERTY()
	TArray<FVector> Vectors;

	//Used for testing custom callstack processing to go from BP->C++->BP->C++
	//A BP Derived class should implement this and call TestCallstackReturn
	UFUNCTION(BlueprintImplementableEvent)
	void TestBlueprintCallstack();

	
	void TestCallstack();

	UFUNCTION(BlueprintCallable, Category="Test")
	FORCENOINLINE bool TestCallstackReturn();

protected:
	virtual void BeginPlay() override;	

	DebugRegisterIndex TestCallstackBreakpoint;
};
