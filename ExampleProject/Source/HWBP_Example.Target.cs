// Copyright 2020 Daniel Amthauer. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class HWBP_ExampleTarget : TargetRules
{
	public HWBP_ExampleTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "HWBP_Example" } );
	}
}
