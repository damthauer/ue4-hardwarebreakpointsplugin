# Hardware Breakpoints
![UE4](https://img.shields.io/badge/UE4-4.24%2B-orange)

This plugin allows users to place data breakpoints during runtime in a simple way from Blueprints or C++. 
Data breakpoints can help you detect which part of your code or blueprints is modifying a variable in your object. This can help you track down difficult bugs.

## Installation:

### From HardwareBreakpoints_1.x.x_for_4.2x.x.zip file:

Unzip the contents of the .zip file to your project's Plugins folder, or to the Plugins/Marketplace folder on your UE4 installation

### From repository download:

Copy the contents of the Plugin folder, to your project's Plugins folder, or to the Plugins/Marketplace folder on your UE4 installation

## Usage notes:

* Some functionality is dependent on having the **Engine Symbols for debugging** installed. Check the *Options* for your installed UE4 version from the Epic Games Launcher.

![Engine Options](https://bitbucket.org/damthauer/ue4-hardwarebreakpointsplugin/raw/ed9a0d81f1f56f99a6890a3a174c98b980c8a5f7/Docs/Images/EngineOptions.png)

![Engine Symbols for debugging](https://bitbucket.org/damthauer/ue4-hardwarebreakpointsplugin/raw/ed9a0d81f1f56f99a6890a3a174c98b980c8a5f7/Docs/Images/EngineSymbols.png)


Licensed under the terms of the Apache License 2.0
